package com.citi.training.employees.rest;

import java.util.List;

import org.apache.juli.logging.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.employees.dao.EmployeeDao;
import com.citi.training.employees.model.Employee;

@RestController
@RequestMapping("/employees")
public class EmployeeController {
	
	private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeDao employeeDao;

    @RequestMapping(method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public List<Employee> findAll(){
    	LOG.debug("findAll() was called");
        return employeeDao.findAll();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.GET,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public Employee findById(@PathVariable int id) {
    	LOG.debug("findById() was called, id: " + id);
        return employeeDao.findById(id);
    }

    @RequestMapping(method=RequestMethod.POST,
                    consumes=MediaType.APPLICATION_JSON_VALUE,
                    produces=MediaType.APPLICATION_JSON_VALUE)
    public HttpEntity<Employee> create(@RequestBody Employee employee) {
    	LOG.debug("create() was called");
        return new ResponseEntity<Employee>(employeeDao.create(employee),
                                            HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
    	LOG.debug("deleteById() was called, id: " + id);
        employeeDao.deleteById(id);
    }
}
