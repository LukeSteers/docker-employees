package com.citi.training.employees.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles("h2")
public class MysqlEmployeeDaoTests {

	@Autowired
	MysqlEmployeeDao mysqlEmployeeDao;
	

	
	@Test
	@Transactional
	public void test_createAndFindAll() {
		int b =mysqlEmployeeDao.findAll().size();
		//System.out.println("CaFA1"+mysqlEmployeeDao.findAll());
			mysqlEmployeeDao.create(new Employee(-1,"Frank",10.0));
			//System.out.println("CaFa2"+mysqlEmployeeDao.findAll());
			assertEquals(mysqlEmployeeDao.findAll().size(),b+1);}
			//mysqlEmployeeDao.deleteById(1);}

	@Test
	@Transactional
	public void test_findById() {
		//System.out.println("FBI1"+mysqlEmployeeDao.findAll());
		mysqlEmployeeDao.create(new Employee(-1,"Frank",10.0));
		//System.out.println("FBI2"+mysqlEmployeeDao.findAll());
		assertEquals(mysqlEmployeeDao.findById(1).getId(),1);
		//System.out.println("FBI3"+mysqlEmployeeDao.findAll());
		//mysqlEmployeeDao.deleteById(1);
		}
		
	@Test
	@Transactional
	public void test_createAndDelete() {
		int a = mysqlEmployeeDao.findAll().size();
		//System.out.println("CaD1"+mysqlEmployeeDao.findAll());
		mysqlEmployeeDao.create(new Employee(-1,"Frank",10.0));
		//System.out.println("CaD2"+mysqlEmployeeDao.findAll());
		mysqlEmployeeDao.create(new Employee(-1,"FrankB",10.0));
		//System.out.println("CaD3"+mysqlEmployeeDao.findAll());
		mysqlEmployeeDao.deleteById(3);
		//System.out.println("CaD4"+mysqlEmployeeDao.findAll());
		assertEquals(mysqlEmployeeDao.findAll().size(),a+1);
		//mysqlEmployeeDao.deleteById(2);
	}

}
